package org.osada.initializer;

import org.osada.models.Klient;
import org.osada.models.Role;
import org.osada.repository.KlientRepository;
import org.osada.repository.ZadaniaRepository;
import org.osada.repository.ZlecenieRepository;
import org.osada.models.Zadanie;
import org.osada.models.Zlecenie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.*;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    private KlientRepository klientRepository;

    @Autowired
    private ZlecenieRepository zlecenieRepository;

    @Autowired
    private ZadaniaRepository zadaniaRepository;

    private Random random = new Random();

    @Override
    public void run(String... args) throws Exception {
        final int iloscZlecen = 2;
        final int iloscZadan = 2;

        Klient klient = new Klient();
        klient.setUsername("admin");
        klient.setRole(Role.ADMIN);
        klientRepository.save(klient);

        Klient user = new Klient();
        user.setUsername("queue");
        user.setRole(Role.USER);
        klientRepository.save(user);

        List<Zadanie> zadania = createTasks();
        zadania.stream().forEach(zadaniaRepository::save);

        for (int i = 0; i < iloscZlecen; i++) {
            Zlecenie zlecenie = randomZlecenie(klient);
            for (int j = 0; j <= iloscZadan; j++) {
                Zadanie zadanie = getRandomZadanie();
                zlecenie.addZadanie(zadanie);
            }
            zlecenieRepository.save(zlecenie);
        }
    }

    private Zadanie getRandomZadanie() {
        int size = zadaniaRepository.findAll().size();
        Long id = Long.valueOf(random.nextInt(size) + 1);
        return zadaniaRepository.getOne(id);
    }

    private List<Zadanie> createTasks() {
        List<Zadanie> zadania = new ArrayList<>();

        Zadanie zadanie1 = new Zadanie();
        zadanie1.setNazwa("Zadanie 1");
        zadanie1.setCena(20.0);
        zadanie1.setWaga(10);
        zadanie1.setCzasTrwania(new Long(16000000));
        zadania.add(zadanie1);

        Zadanie zadanie2 = new Zadanie();
        zadanie2.setNazwa("Zadanie 2");
        zadanie2.setCena(210.0);
        zadanie2.setWaga(12);
        zadanie2.setCzasTrwania(new Long(16000000));
        zadania.add(zadanie2);

        return zadania;
    }

    private Zlecenie randomZlecenie(Klient klient) {
        Zlecenie zlecenie = new Zlecenie();
        zlecenie.setNazwa("zlecenie" + random.nextInt());
        zlecenie.setDeadline(new Long(1494150167));
        zlecenie.setKlient(klient);
        return zlecenie;
    }
}
