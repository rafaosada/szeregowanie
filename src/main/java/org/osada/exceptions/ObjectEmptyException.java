package org.osada.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Brak zalogowanego klienta")
public class ObjectEmptyException extends RuntimeException{
}
