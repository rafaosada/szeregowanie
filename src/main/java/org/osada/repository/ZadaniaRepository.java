package org.osada.repository;

import org.osada.models.Zadanie;
import org.osada.models.Zlecenie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ZadaniaRepository extends JpaRepository<Zadanie, Long>{
}
