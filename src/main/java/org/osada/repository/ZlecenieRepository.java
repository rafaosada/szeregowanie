package org.osada.repository;

import org.osada.models.Klient;
import org.osada.models.Zlecenie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ZlecenieRepository extends JpaRepository<Zlecenie, Long> {
    List<Zlecenie> getZleceniaByKlient(Klient klient);
}
