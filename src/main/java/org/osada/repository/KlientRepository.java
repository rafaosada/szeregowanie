package org.osada.repository;

import org.osada.models.Klient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface KlientRepository extends JpaRepository<Klient, Long> {
    Klient getKlientByUsername(String username);
}
