package org.osada.algorithm;

import org.osada.models.Zlecenie;
import org.osada.repository.ZlecenieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AlgorytmSzeregowania {

    @Autowired
    private ZlecenieRepository zlecenieRepository;

    public List<Zlecenie> szereguj() {
        List<Zlecenie> zleceniaDoSzeregowania = this.zlecenieRepository.findAll();

        HashMap<Zlecenie, Integer> unsortedMap =
                zleceniaDoSzeregowania.stream().
                        collect(HashMap<Zlecenie, Integer>::new,
                                (m, z) -> m.put(z, z.wezWagaZlecenia()),
                                (m, z) -> {
                                });

        List<Zlecenie> poszeregowaneZlecenia = unsortedMap.entrySet().stream()
                .sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .map(e -> e.getKey())
                .collect(Collectors.toList());

        long tempTime = new Date().getTime();

        for (Zlecenie zlecenie : poszeregowaneZlecenia) {
            tempTime += zlecenie.wezCzasTrwaniaZlecenia();
            zlecenie.setCalcDeadline(tempTime);
            System.out.println(zlecenie);
            long spoznienie = zlecenie.getCalcDeadline() - zlecenie.getDeadline();
            spoznienie = spoznienie < 0 ? 0 : spoznienie;
            spoznienie /= 86400000;
            double rabat = (spoznienie * zlecenie.wezWagaZlecenia());
            rabat = rabat > 20 ? 20 : rabat;
            zlecenie.setRabat(rabat);
            zlecenieRepository.save(zlecenie);
        }

        return poszeregowaneZlecenia;
    }

}
