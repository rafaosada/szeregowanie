package org.osada;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SzeregowanieApplication {

	public static void main(String[] args) {
		SpringApplication.run(SzeregowanieApplication.class, args);
	}
}
