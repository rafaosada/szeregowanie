package org.osada.models;

import org.osada.repository.KlientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class CurrentlyLogin {

    @Autowired
    private KlientRepository klientRepository;

    private String username;

    public String getUsername() {
        return username;
    }

    public Klient getKlient() {
        return klientRepository.getKlientByUsername(this.username);
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
