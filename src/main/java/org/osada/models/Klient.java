package org.osada.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Klienci")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Klient {

    @Id
    @Column(unique=true)
    private String username;

    private String imie;

    private String nazwisko;

    private String email;

    private String role;

    @JsonIgnore
    @OneToMany(mappedBy = "klient", fetch = FetchType.LAZY)
    private List<Zlecenie> zlecenia;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Zlecenie> getZlecenia() {
        return zlecenia;
    }

    public void setZlecenia(List<Zlecenie> zlecenia) {
        this.zlecenia = zlecenia;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Klient{" +
                "username='" + username + '\'' +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", email='" + email + '\'' +
                ", zlecenia=" + zlecenia +
                '}';
    }
}
