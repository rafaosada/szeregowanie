package org.osada.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Zlecenia")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Zlecenie {

    @Id
    @Column(name="ID")
    @GeneratedValue
    private Long id;

    private String nazwa;

    private Long deadline;

    private Long calcDeadline;

    private Double rabat;

    //@JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "heheszki",
            joinColumns = @JoinColumn(name = "zlecenie_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "zadanie_id", referencedColumnName = "ID"))
    private List<Zadanie> zadania = new ArrayList<>();

    //@JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="KLIENT_ID")
    private Klient klient;

    public void addZadanie(Zadanie zadanie) {
        if(this.zadania.indexOf(zadanie) == -1) {
            this.zadania.add(zadanie);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Long getDeadline() {
        return deadline;
    }

    public void setDeadline(Long deadline) {
        this.deadline = deadline;
    }

    public List<Zadanie> getZadania() {
        return zadania;
    }

    public void setZadania(List<Zadanie> zadania) {
        this.zadania = zadania;
    }

    public Klient getKlient() {
        return klient;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    public Long getCalcDeadline() {
        return calcDeadline;
    }

    public void setCalcDeadline(Long calcDeadline) {
        this.calcDeadline = calcDeadline;
    }


    public Double getRabat() {
        return rabat;
    }

    public void setRabat(Double rabat) {
        this.rabat = rabat;
    }


    public Integer wezWagaZlecenia() {
        int size = this.zadania.size();

        return size == 0 ? 0 : size + this.zadania.stream().mapToInt(Zadanie::getWaga).sum() / size;
    }

    public Long wezCzasTrwaniaZlecenia() {
        return this.zadania.stream().mapToLong(zadanie -> zadanie.getCzasTrwania()).sum();
    }
}