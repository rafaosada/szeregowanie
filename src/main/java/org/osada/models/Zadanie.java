package org.osada.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Zadania")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Zadanie {

    @Id
    @Column(name="ID")
    @GeneratedValue
    private Long id;

    @Column(unique=true)
    private String nazwa;

    private Integer waga;

    private Long czasTrwania;

    private Double cena;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Integer getWaga() {
        return waga;
    }

    public void setWaga(Integer waga) {
        this.waga = waga;
    }

    public Long getCzasTrwania() {
        return czasTrwania;
    }

    public void setCzasTrwania(Long czasTrwania) {
        this.czasTrwania = czasTrwania;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getId() == ((Zadanie)obj).getId();
    }

    @Override
    public String toString() {
        return "Zadanie{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", waga=" + waga +
                ", czasTrwania=" + czasTrwania +
                ", cena=" + cena +
                '}';
    }
}
