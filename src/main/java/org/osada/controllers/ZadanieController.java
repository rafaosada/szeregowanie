package org.osada.controllers;

import org.osada.exceptions.KlientNotExistsException;
import org.osada.exceptions.ObjectEmptyException;
import org.osada.models.CurrentlyLogin;
import org.osada.models.Zadanie;
import org.osada.models.Zlecenie;
import org.osada.repository.ZadaniaRepository;
import org.osada.repository.ZlecenieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "zadania")
public class ZadanieController {

    @Autowired
    private CurrentlyLogin currentlyLogin;

    @Autowired
    private ZadaniaRepository zadaniaRepository;

    @Autowired
    private ZlecenieRepository zlecenieRepository;

    static Logger logger = Logger.getLogger(ZadanieController.class.getName());

    @GetMapping(value = "all")
    public List<Zadanie> getAllZadania() {
        return zadaniaRepository.findAll();
    }

    @GetMapping(value = "{zlecenieId}")
    public List<Zadanie> getZadaniaByZlecenieId(@PathVariable Long zlecenieId) {
        Zlecenie zlecenie = getZlecenieByZlecenieId(zlecenieId);
        if (zlecenie == null) {
            throw new ObjectEmptyException();
        }
        return zlecenie.getZadania();
    }

    @PostMapping("/{idZlecenia}")
    public void addZadanie(@PathVariable("idZlecenia") Long idZlecenia, @RequestBody Zadanie zadanie) {
        Zlecenie zlecenie = zlecenieRepository.getOne(idZlecenia);
        zlecenie.setCalcDeadline(null);
        zlecenie.setRabat((double) 0);
        zlecenie.addZadanie(zadanie);
        zlecenieRepository.save(zlecenie);
    }

    @PostMapping
    public Zadanie addNewZadanie(@RequestBody Zadanie zadanie) {
        return zadaniaRepository.save(zadanie);
    }

    @DeleteMapping(value = "{idZlecenia}/{idZadania}")
    public void deleteZadanie(@PathVariable("idZlecenia") Long idZlecenia, @PathVariable("idZadania") Long idZadania){
        Zlecenie zlecenie = zlecenieRepository.getOne(idZlecenia);
        Zadanie zadanie = zadaniaRepository.getOne(idZadania);
        zlecenie.setCalcDeadline(null);
        zlecenie.setRabat((double) 0);
        zlecenie.getZadania().remove(zadanie);
        zlecenieRepository.save(zlecenie);
    }

    private Zlecenie getZlecenieByZlecenieId(Long zlecenieId) {
        return zlecenieRepository.getOne(zlecenieId);
    }

}
