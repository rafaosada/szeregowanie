package org.osada.controllers;

import org.osada.algorithm.AlgorytmSzeregowania;
import org.osada.models.Zlecenie;
import org.osada.repository.ZlecenieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "szeregowanie")
public class SzeregowanieController {

    @Autowired
    private AlgorytmSzeregowania algorytmSzeregowania;

    @GetMapping
    public List<Zlecenie> szereguj() {
        return algorytmSzeregowania.szereguj();
    }

}
