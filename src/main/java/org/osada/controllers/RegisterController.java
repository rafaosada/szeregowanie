package org.osada.controllers;

import org.osada.models.Klient;
import org.osada.models.Role;
import org.osada.repository.KlientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "register")
public class RegisterController {

    @Autowired
    private KlientRepository klientRepository;

    @PostMapping
    public void register(@RequestBody Klient newUser) {
        newUser.setRole(Role.USER);
        if (klientRepository.getKlientByUsername(newUser.getUsername()) != null) {
            throw new RuntimeException("Użytkownik istnieje");
        }
        klientRepository.save(newUser);
    }

}
