package org.osada.controllers;

import org.osada.exceptions.KlientNotExistsException;
import org.osada.models.CurrentlyLogin;
import org.osada.models.Klient;
import org.osada.repository.KlientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping(value = "login")
public class LoginController {

    @Autowired
    private CurrentlyLogin currentlyLogin;

    @Autowired
    private KlientRepository klientRepository;

    static Logger logger = Logger.getLogger(LoginController.class.getName());

    @PostMapping(value = "{username}")
    private Klient login(@PathVariable String username) {
        Klient klient = klientRepository.getKlientByUsername(username);
        if (klient != null) {
            currentlyLogin.setUsername(username);
            logger.info("Zalogowano: " + username);
        }
        return klient;
    }

    @PostMapping(value = "logout")
    private void logout() {
        currentlyLogin.setUsername("");
    }

}
