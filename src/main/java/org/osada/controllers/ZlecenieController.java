package org.osada.controllers;

import org.osada.algorithm.AlgorytmSzeregowania;
import org.osada.exceptions.KlientNotExistsException;
import org.osada.exceptions.ObjectEmptyException;
import org.osada.models.CurrentlyLogin;
import org.osada.models.Klient;
import org.osada.models.Zlecenie;
import org.osada.repository.KlientRepository;
import org.osada.repository.ZlecenieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "zlecenia")
public class ZlecenieController {

    private static final int SZEREGUJ_PO = 3;
    private int iterator = 0;

    @Autowired
    private AlgorytmSzeregowania algorytmSzeregowania;

    @Autowired
    private CurrentlyLogin currentlyLogin;

    @Autowired
    private ZlecenieRepository zlecenieRepository;

    @Autowired
    private KlientRepository klientRepository;

    static Logger logger = Logger.getLogger(ZlecenieController.class.getName());

    @GetMapping
    public List<Zlecenie> getZlecenia() {
        if (currentlyLogin.getUsername().isEmpty()) {
            throw new ObjectEmptyException();
        } else {
            return zlecenieRepository.getZleceniaByKlient(getKlientByUsername(currentlyLogin.getUsername()));
        }
    }

    @PostMapping
    public Zlecenie addZlecenie(@RequestBody Zlecenie zlecenie) {
        zlecenie.setKlient(currentlyLogin.getKlient());
        Zlecenie result = zlecenieRepository.save(zlecenie);
        iterator++;
        if(iterator > SZEREGUJ_PO) {
            algorytmSzeregowania.szereguj();
            iterator = 0;
        }
        return result;
    }

    @DeleteMapping(value = "{id}")
    public void deleteZlecenie(@PathVariable Long id){
        zlecenieRepository.delete(id);
    }

    private Klient getKlientByUsername(String username){
        Klient klient = klientRepository.getKlientByUsername(username);
        if (klient == null) {
            throw new KlientNotExistsException();
        }
        return klient;
    }

}
