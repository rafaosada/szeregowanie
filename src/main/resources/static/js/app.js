let app = angular.module("szeregowanie", []);

app.controller("mainController", ($scope, AuthService, OrderService, TaskService, AlgorytmSzeregowania) => {
    $scope.logged = false;

    $scope.isAdmin = () => {
        return $scope.user && $scope.user.role === "ADMIN";
    }

    $scope.login = (username) => {
        AuthService.login(username).then(
            (response) => {
                if (response.data) {
                    $scope.logged = true;
                    $scope.user = response.data;
                    updateOrders();
                }
            }
        )
    }

    $scope.logout = () => {
        $scope.user = {};
        $scope.order = {};
        $scope.logged = false;
    }

    $scope.szereguj = () => {
        AlgorytmSzeregowania.szereguj().then(
            (response) => {
                $("#szeregowanie-modal").modal('show');
                $scope.poszeregowaneZlecenia = response.data;
                updateOrders();
            }
        );
    }

    $scope.adminDeleteOrder = (orderId) => {
        OrderService.remove(orderId).then(
            () => {
                updateOrders();
                $scope.szereguj();
            }
        );
    }

    $scope.timestampToDate = (timestamp) => {
        return new Date(timestamp * 1000).toUTCString();
    }

    $scope.dateToTimestamp = (date) => {
        return Date.parse(date)
    }

    $scope.addNewOrder = (newOrder) => {
        $scope.newOrder.deadline = $scope.dateToTimestamp($scope.newOrder.deadline)/1000;
        $scope.newOrder.czasTrwania = $scope.newOrder.czasTrwania * 3600000;
        OrderService.addOrder(newOrder).then(
            () => {
                $("#new-order-modal").modal('hide');
                updateOrders();
            }
        );
    }

    $scope.deleteOrder = (id) => {
        OrderService.remove(id).then(
            () => {
                updateOrders();
                if($scope.order && $scope.order.orderId === id) {
                    $scope.order = undefined;
                }
            }
        );
    }

    $scope.addTask = () => {
        let tasks = $scope.order;
        TaskService.addTask(tasks.orderId, tasks.selected).then(
            (response) => {
                let order = $scope.order;
                updateTasks(order.orderId, order.orderName);
                updateOrders();
            }
        )
    }

    $scope.deleteTask = (idOrder, idTask) => {
        TaskService.remove(idOrder, idTask).then(
            () => {
                let order = $scope.order;
                updateTasks(order.orderId, order.orderName);
                updateOrders();
            }
        );
    }

    $scope.getAllTasks = () => {
        return TaskService.getAllTasks().then(
            (response) => $scope.allTasks = response.data
        );
    }

    $scope.getTask = (orderId, orderName) => {
        updateTasks(orderId, orderName);
    }

    $scope.createNewTask = (task) => {
        task.czasTrwania * 3600000;
        TaskService.createNewTask(task).then(
            () => {
                $("#create-new-task-modal").modal('hide');
                $scope.getAllTasks();
            }
        );
    }

    let updateOrders = () => {
        OrderService.getOrders().then(
            (response) => {
                $scope.orders = response.data;
                $scope.orders.forEach( (order) => {
                    let kwota = 0;
                    order.zadania.forEach( (task) => {
                       kwota += task.cena;
                    });
                    order.kwota = kwota;
                });
            }
        )
    }

    let updateTasks = (orderId, orderName) => {
        TaskService.getTasks(orderId).then(
            (response) => {
                $scope.order = {
                    orderName: orderName,
                    orderId: orderId,
                    tasks: response.data
                };
            }
        );
    }

    $scope.getAllTasks();

    $("#deadlinePicker").on("dp.change", function() {
        $scope.newOrder.deadline = $("#deadlinePicker input").val();
    });
});

app.controller("registerController", ($scope, $http) => {
    $scope.newUser = {};
    $scope.register = (newUser) => {
        $scope.errorMessage = undefined;
        $http.post("register", newUser).then(
            () => $("#register-modal").modal('hide'),
            () => $scope.errorMessage = "Użytkownik o podanym username istnieje!"
        );
    }
});

app.service("AlgorytmSzeregowania", function ($http) {
    this.szereguj = () => {
        return $http.get("/szeregowanie");
    }
});

app.service("AuthService", function ($http) {
    this.login = (username) => {
        return $http.post("/login/" + username);
    }

    this.logout = () => {
        return $http.post("/logout");
    }
});

app.service("OrderService", function ($http) {
    this.addOrder =(order) => {
        return $http.post("zlecenia", order);
    }

    this.getOrders = () => {
        return $http.get("/zlecenia");
    }

    this.remove = (id) => {
        return $http.delete("/zlecenia/" + id);
    }
});

app.service("TaskService", function ($http) {
    this.getAllTasks = () => {
        return $http.get("/zadania/all");
    }

    this.getTasks = (orderId) => {
        return $http.get("/zadania/" + orderId);
    }

    this.addTask = (orderId, task) => {
        return $http.post("/zadania/" + orderId, task);
    }

    this.remove = (idOrder, idTask) => {
        return $http.delete("/zadania/" + idOrder + "/" + idTask);
    }

    this.createNewTask = (task) => {
        return $http.post("/zadania/", task);
    }
});
